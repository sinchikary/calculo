package org.fundacionjala;

public class bFunction implements IFunction {

    @Override
    public float f(float x) {
        float result = 100*Math.abs(x)/x;
        return result;
    }
}