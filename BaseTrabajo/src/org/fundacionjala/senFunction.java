package org.fundacionjala;

public class senFunction implements IFunction{

    @Override
    public float f(float x) {
        Float result = (float) ( 100F * Math.sin(x * Math.PI / 100F));
        return result;
    }
   
}