package org.fundacionjala;

import javax.imageio.ImageIO;



import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FunctionPlotter {
    private int imageWidth;
    private int imageHeight;
    private BufferedImage image;
    private Graphics2D g;

    public FunctionPlotter(int imageWidth, int imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    public void prepare() {
        this.g.setColor(Color.white);
        this.g.fillRect(0, 0, this.imageWidth, this.imageHeight);
        this.g.setColor(Color.blue);
        this.g.drawString("y", 240, 20);
        this.g.drawString("x",490,260);
        this.g.setColor(Color.gray);
        for (int i = 0; i < imageWidth; i += 50) {
            this.g.drawLine(i, 0, i, 500);
            
        }
        for (int j = 0; j < imageHeight; j += 50) {
            this.g.drawLine(0, j, 500, j);
            
        }
        paintLines();
        this.g.setColor(Color.red);
        this.g.drawLine(0,250,500,250);
        this.g.drawLine(250, 0, 250, 500);
    }

    public void plot(IFunction function) {
        float domainFrom = -imageWidth/2;
        float domainTo = imageHeight/2;

        int lastU = 0;
        int lastV = 0;
        int c = -imageWidth/2;

        boolean firstPoint = true;
        boolean invalid = false;

        this.g.setColor(Color.blue);
        for(float x = (float)c; x <= domainTo; x+=1) {
           
            while(Float.isNaN(function.f(x))){
                invalid = true;
                firstPoint = true;
                x+=1;
            } 
            if(Float.isNaN(function.f(x-1)) || Float.isInfinite(function.f(x-1))) {
                invalid = true;
                firstPoint = true;
            } 
            float y = -function.f(x);
            // You need to take a look to your homework
            int u = (int)x + 250;
            int v = (int)y + 250;

            if(invalid){
                this.g.drawOval((int)x+249-4, 249-(int)function.f(x)-4, 8, 8); 
                lastU = (int)x+250;
                lastV =  250-(int)function.f(x);
                invalid = false;
            }
            if (!firstPoint) {
               this.g.drawLine(lastU, lastV, u, v);
            } else {
                firstPoint = false;
            }
            if(!invalid && !firstPoint){
                lastU = u;
                lastV = v;
            }
        }
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }

    private void paintLines() {
        for (int i = -250; i < imageWidth/2; i+=50) {
            this.g.drawString(""+ i, i+250, 249);
            this.g.drawString(""+ -i, 250, i+250);    
        }
       
        
    }
}
