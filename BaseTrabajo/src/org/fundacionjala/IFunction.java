package org.fundacionjala;

public interface IFunction {
    float f(float x);
}
