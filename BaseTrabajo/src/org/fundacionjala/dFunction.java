package org.fundacionjala;

public class dFunction implements IFunction{

    @Override
    public float f(float x) {
        float result = (float) (2000 * Math.sin(x / 10) / x);
        return result;   
    }
    
}