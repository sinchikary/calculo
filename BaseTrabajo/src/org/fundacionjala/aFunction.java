package org.fundacionjala;

public class aFunction implements IFunction{

    @Override
    public float f(float x) {
        float result = (float) (x * Math.sqrt(10) / (Math.sqrt(x + 10) - Math.sqrt(10)));
        return result;
    }
    
}