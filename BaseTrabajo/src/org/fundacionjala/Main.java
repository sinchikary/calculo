package org.fundacionjala;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        int imageWidth = 500;
        int imageHeight = 500;
        String pngPath = "my-function.png";

        FunctionPlotter plotter = new FunctionPlotter(imageWidth, imageHeight);
        plotter.prepare();
        plotter.plot(new dFunction());
        plotter.save(pngPath);

        System.out.println("The image was generated at location: " + pngPath);
    }
}
